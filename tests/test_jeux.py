import pytest
import re
from project.jeux import Jeux


@pytest.fixture
def ex_kwargs():
    return dict(identifiant=14691600,
                nom="FN",
                date="17/06/2026",
                genre="Tir",
                prix=20,
                developpeur="bob",
                description="cool",
                poucebleu=1,
                poucerouge=10,
                note=20,
                nombre_dlc=0,
                windows=True,
                mac=True,
                linux=True,
                temps_moyen=0,
                temps_moyen_2semaines=0,
                age_limite=18)


@pytest.mark.parametrize(
    "kwargs, erreur, message_erreur",
    [
        (
            {"description": ["description du jeu"]},
            TypeError,
            "La description  doit être une chaîne de caractères.",
        ),
        (
            {"poucebleu": 3.14},
            TypeError,
            "Poucebleu doit être un entier.",
        ),
        (
            {"poucerouge": 3.14},
            TypeError,
            "Poucerouge doit être un entier.",
        ),
        (
            {"note": "-56"},
            TypeError,
            "Note doit être un flottant.",
        ),
        (
            {"note": 153},
            ValueError,
            "Les notes doivent être inférieures à 100.",
        ),
        (
            {"nombre_dlc": 20.5},
            TypeError,
            "Le nombre de DLC doit être un entier.",
        ),
        (
            {"windows": "Yes"},
            TypeError,
            "Windows doit être un booléen.",
        ),
        (
            {"mac": "Yes"},
            TypeError,
            "Mac doit être un booléen.",
        ),
        (
            {"linux": "Yes"},
            TypeError,
            "Linux doit être un booléen.",
        ),
        (
            {"temps_moyen": "3.14"},
            TypeError,
            "Le temps de jeu moyen doit être un nombre flottant.",
        ),
        (
            {"temps_moyen_2semaines": "3.14"},
            TypeError,
            "Le temps de jeu moyen après la sortie doit" +
            "être un nombre flottant.",
        ),
        (
            {"age_limite": 18.5},
            TypeError,
            "L'âge limite doit être un entier.",
        ),
    ],
)
def test_jeux_invalid_input(ex_kwargs, kwargs, erreur, message_erreur):
    ex_kwargs.update(kwargs)
    with pytest.raises(erreur, match=re.escape(message_erreur)):
        Jeux(**ex_kwargs)

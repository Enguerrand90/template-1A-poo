import pytest
from project.recherche import Recherche


@pytest.mark.parametrize(
    "attribut, valeurattrib, erreur, resultat",
    [
        (50, 'attribut_test', TypeError, "L'attribut doit"
         " être une chaîne de caractères."),
        (['test'], 'attribut_test', TypeError, "L'attribut doit"
         " être une chaîne de caractères."),
        (("test"), 'attribut_test', TypeError, "L'attribut doit"
         " être une chaîne de caractères."),
    ]
)
<<<<<<< HEAD
def test_recherche_init(attribut, valeurattrib, erreur,
                        resultat):
=======
def test_recherche_init(attribut, valeurattrib, erreur, resultat):
>>>>>>> 3860384a63b1e9f12085b8a5fa32989f76749dc7
    if erreur is not None:
        with pytest.raises(erreur, match=resultat):
            Recherche(attribut, valeurattrib)
    else:
        assert Recherche(attribut, valeurattrib) == resultat

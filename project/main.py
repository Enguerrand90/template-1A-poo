import csv

from InquirerPy import prompt

from project.admin import Admin
from project.invite import Invite
from project.jeux import Jeux
from project.recherche import Recherche
from project.utilisateurConnecte import Utilisateurconnecte

print("Bonjour et bienvenue sur Stim!")

actif = 1

while actif == 1:
    """BLOC PRINCIPAL"""
    questions = [
        {
            "type": "list",
            "message": "Que voulez-vous faire?",
            "choices": [
                "Se connecter",
                "Créer un compte",
                "Rechercher des jeux",
                "Quitter Stim",
            ],
        },
    ]

    result = prompt(questions)
    choix = result[0]

    if choix == "Se connecter":
        """BLOC SE CONNECTER"""
        questions = [
            {
                "type": "number",
                "message": "Quel est votre identifiant?",
                "name": "userid",
            },
        ]
        result = prompt(questions)
        userid = int(result["userid"])
        Invite().connexion(userid)
        numligneuser = Invite().numligneuser(userid)
        if Invite().connexion(userid) == "admin":
            with open("ressources/bddutilisateurs.csv", "r",
                      newline="") as infile:
                reader = csv.reader(infile)
                rows = list(reader)
                userid = rows[numligneuser][0]
                pseudo = rows[numligneuser][1]
                age = rows[numligneuser][2]
                wishlist = list(rows[numligneuser][3])
                listenote = list(rows[numligneuser][5])
                listepoucebleu = list(rows[numligneuser][6])
                listepoucerouge = list((rows[numligneuser][7]))

            admin = Admin(
                int(userid),
                pseudo,
                int(age),
                wishlist,
                listenote,
                listepoucebleu,
                listepoucerouge,
            )
            admin.wishlist = admin.convertisseur_wishlist()
            admin.wishlist = admin.supcrochetwishlist()
            admin.wishlist = admin.supguillemetwishlist()

            while admin.st == 1:
                """BLOC QUE VOULEZ-VOUS FAIRE ADMIN"""
                questions = [
                    {
                        "type": "list",
                        "message": "Que voulez-vous faire?",
                        "choices": [
                            "Ajouter un jeu",
                            "Modifier un jeu",
                            "Supprimer un jeu",
                            "Rechercher des jeux",
                            "Afficher votre wishlist",
                            "Ajouter un jeu à votre wishlist",
                            "Supprimer un jeu de votre wishlist",
                            "Ajouter une note à un jeu",
                            "Ajouter un pouce bleu à un jeu",
                            "Ajouter un pouce rouge à un jeu",
                            "Enlever un" " pouce bleu à un jeu",
                            "Enlever un pouce rouge à un jeu",
                            "Afficher le pourcentage de pouces bleu",
                            "Se deconnecter",
                        ],
                    },
                ]

                result = prompt(questions)
                choix = result[0]

                if choix == "Ajouter un jeu":
                    """BLOC AJOUTER JEU"""
                    questions = [
                        {"type": "input", "message": "Quel est le nom?",
                                 "name": "nom"},
                        {
                            "type": "input",
                            "message": "Quel est la date de sortie?",
                            "name": "date",
                        },
                        {
                            "type": "input",
                            "message": "Quel est le genre?",
                            "name": "genre",
                        },
                        {
                            "type": "number",
                            "message": "Quel est le prix?",
                            "name": "prix",
                        },
                        {
                            "type": "input",
                            "message": "Quel(s) est(sont)"
                                       " le(s) développeur(s)?",
                            "name": "developpeur",
                        },
                        {
                            "type": "input",
                            "message": "Quelle est la description du jeu?",
                            "name": "description",
                        },
                        {
                            "type": "number",
                            "message": "Quel est le nombre de DLC?",
                            "name": "nombre_dlc",
                        },
                        {
                            "type": "list",
                            "message": "Le jeu est-il"
                                       " disponible sous windows?",
                            "choices": ["True", "False"],
                            "name": "windows",
                        },
                        {
                            "type": "list",
                            "message": "Le jeu est-il disponible sous Mac?",
                            "choices": ["True", "False"],
                            "name": "mac",
                        },
                        {
                            "type": "list",
                            "message": "Le jeu est-il disponible sous Linux?",
                            "choices": ["True", "False"],
                            "name": "linux",
                        },
                        {
                            "type": "number",
                            "message": "Quel est l'âge requis pour jouer?",
                            "name": "age",
                        },
                    ]

                    result = prompt(questions)

                    admin.ajouter_jeux(
                        result["nom"],
                        result["date"],
                        result["genre"],
                        float(result["prix"]),
                        result["developpeur"],
                        result["description"],
                        int(result["nombre_dlc"]),
                        bool(result["windows"]),
                        bool(result["mac"]),
                        bool(result["linux"]),
                        int(result["age"]),
                        temps_moyen=0.0,
                        temps_moyen_2semaine=0.0,
                        poucebleu=0,
                        poucerouge=0,
                        note=0.0,
                    )

                elif choix == "Modifier un jeu":
                    """BLOC MODIFIER JEU"""
                    questions = [
                        {
                            "type": "number",
                            "message": "Quel est l'identifiant du jeu?",
                            "name": "id",
                        },
                        {
                            "type": "list",
                            "message": "Quel est l'attribut à modifier?",
                            "choices": [
                                "nom",
                                "date",
                                "genre",
                                "prix",
                                "developpeur",
                                "description",
                                "poucebleu",
                                "poucerouge",
                                "note",
                                "nombre_dlc",
                                "windows",
                                "mac",
                                "linux",
                                "temps_moyen",
                                "temps_moyen_2semaine",
                                "age_limite",
                            ],
                            "name": "attribut",
                        },
                        {
                            "type": "input",
                            "message": "Quelle est la valeur"
                                       " du nouvel attribut?",
                            "name": "nouvelattrib",
                        },
                    ]
                    result = prompt(questions)

                    if result["attribut"] in {
                        "prix",
                        "temps_moyen",
                        "temps_moyen_2semaine",
                    }:
                        result["nouvelattrib"] = float(result["nouvelattrib"])
                        admin.modifier_jeux(
                            int(result["id"]),
                            result["attribut"],
                            result["nouvelattrib"],
                        )
                    elif result["attribut"] in {
                        "nombre_dlc",
                        "age_limite",
                        "poucebleu",
                        "poucerouge",
                    }:
                        result["nouvelattrib"] = int(result["nouvelattrib"])
                        admin.modifier_jeux(
                            int(result["id"]),
                            result["attribut"],
                            result["nouvelattrib"],
                        )
                    elif result["attribut"] in {"windows", "mac", "linux"}:
                        result["nouvelattrib"] = bool(result["nouvelattrib"])
                        admin.modifier_jeux(
                            int(result["id"]),
                            result["attribut"],
                            result["nouvelattrib"],
                        )
                    else:
                        admin.modifier_jeux(
                            int(result["id"]),
                            result["attribut"],
                            result["nouvelattrib"],
                        )

                elif choix == "Supprimer un jeu":
                    """BLOC SUPPRIMER JEU"""
                    questions = [
                        {
                            "type": "number",
                            "message": "Quel est l'identifiant du jeu?",
                            "name": "id",
                        },
                    ]
                    result = prompt(questions)
                    admin.supprimer_jeux(int(result["id"]))

                elif choix == "Rechercher des jeux":
                    """BLOC RECHERCHER JEU ADMIN"""
                    question = [
                        {
                            "type": "list",
                            "message": "Que recherchez-vous ?",
                            "choices": [
                                "Identifiant",
                                "Nom",
                                "Date de sortie",
                                "Genre",
                                "Prix",
                                "Developpeur",
                                "Description",
                                "Nombre de pouces bleu",
                                "Nombre de pouces rouge",
                                "Note (entre 0 et 100)",
                                "Nombre de DLC",
                                "Jeux disponibles sur Windows",
                                "Jeux disponibles sur Mac",
                                "Jeux disponibles sur Linux",
                                "temps de jeu moyen",
                                "temps de jeu moyen 2 semaines après"
                                " la sortie",
                                "Age limite",
                            ],
                        },
                    ]

                    result = prompt(question)
                    attribut = result[0]
                    if attribut == "Prix":
                        question = [
                            {
                                "type": "number",
                                "message": "Quel est le prix minimum?",
                                "name": "prixmin",
                            },
                            {
                                "type": "number",
                                "message": "Quel est le prix maximum?.",
                                "name": "prixmax",
                            },
                        ]
                        result = prompt(question)
                        valeurattrib = (result["prixmin"], result["prixmax"])
                        recherche = Recherche(attribut, valeurattrib)
                        recherche.rechercher()
                    elif attribut == "Note (entre 0 et 100)":
                        question = [
                            {
                                "type": "number",
                                "message": "Quel est la note minimale?",
                                "name": "notemin",
                            },
                            {
                                "type": "number",
                                "message": "Quel est la note maximale?",
                                "name": "notemax",
                            },
                        ]
                        result = prompt(question)
                        valeurattrib = (result["notemin"], result["notemax"])
                        recherche = Recherche(attribut, valeurattrib)
                        recherche.rechercher()
                    else:
                        question = [
                            {
                                "type": "input",
                                "message": f"Precisez {attribut} :",
                                "name": "valeurattrib",
                            }
                        ]
                        result = prompt(question)
                        valeurattrib = result["valeurattrib"]

                        recherche = Recherche(attribut, valeurattrib)
                        recherche.rechercher()

                elif choix == "Afficher votre wishlist":
                    """BLOC AFFICHER WISHLIST ADMIN"""
                    admin.afficher_wishlist()

                elif choix == "Ajouter un jeu à votre wishlist":
                    """BLOC AJOUTER UN JEU WISHLIST ADMIN"""
                    questions = [
                        {
                            "type": "input",
                            "message": "Quel est le nom du jeu"
                            " que vous voulez ajouter ?",
                            "name": "nom",
                        }
                    ]
                    result = prompt(questions)
                    admin.ajouter_wishlist(result["nom"])

                elif choix == "Supprimer un jeu de votre wishlist":
                    """BLOC SUPPRIMER UN JEU WISHLIST ADMIN"""
                    questions = [
                        {
                            "type": "input",
                            "message": "Quel est le nom du jeu"
                            " que vous voulez supprimer ?",
                            "name": "nom",
                        }
                    ]

                    result = prompt(questions)
                    admin.supprimer_wishlist(result["nom"])

                elif choix == "Ajouter une note à un jeu":
                    """BLOC AJOUTER UNE NOTE A UN JEU ADMIN"""
                    questions = [
                        {
                            "type": "number",
                            "message": "Que est l'identifiant du jeu?",
                            "name": "id",
                        },
                        {
                            "type": "number",
                            "message": "Quelle note donnez-vous à ce jeu?"
                            " (Entier entre 0 et 100)",
                            "name": "note",
                        },
                    ]
                    result = prompt(questions)
                    id = result["id"]
                    note = result["note"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.ajouter_note(int(note), int(numligneuser))

                elif choix == "Ajouter un pouce bleu à un jeu":
                    """BLOC AJOUTER UN POUCE BLEU A UN JEU ADMIN"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.ajoute_poucebleu(numligneuser)

                elif choix == "Ajouter un pouce rouge à un jeu":
                    """BLOC AJOUTER UN POUCE ROUGE A UN JEU ADMIN"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.ajoute_poucerouge(numligneuser)

                elif choix == "Enlever un pouce bleu à un jeu":
                    """BLOC ENLEVER UN POUCE BLEU A UN JEU ADMIN"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.supprime_poucebleu(numligneuser)

                elif choix == "Enlever un pouce rouge à un jeu":
                    """BLOC ENLEVER UN POUCE ROUGE A UN JEU ADMIN"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.supprime_poucerouge(numligneuser)

                elif choix == "Afficher le pourcentage de pouces bleu":
                    """BLOC AFFICHER LE POURCENTAGE DE POUCES BLEU ADMIN"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.recommendation_pouce()

                elif choix == "Se deconnecter":
                    """BLOC SE DECONNECTER ADMIN"""
                    admin.deconnexion()

        if Invite().connexion(int(userid)) == "user":
            with open("ressources/bddutilisateurs.csv", "r",
                      newline="") as infile:
                reader = csv.reader(infile)
                rows = list(reader)
                userid = rows[numligneuser][0]
                pseudo = rows[numligneuser][1]
                age = rows[numligneuser][2]
                wishlist = list(rows[numligneuser][3])
                listenote = list(rows[numligneuser][5])
                listepoucebleu = list(rows[numligneuser][6])
                listepoucerouge = list((rows[numligneuser][7]))

            user = Utilisateurconnecte(
                int(userid),
                pseudo,
                int(age),
                wishlist,
                listenote,
                listepoucebleu,
                listepoucerouge,
            )
            user.wishlist = user.convertisseur_wishlist()
            user.wishlist = user.supcrochetwishlist()
            user.wishlist = user.supguillemetwishlist()

            while user.st == 1:
                """BLOC QUE VOULEZ-VOUS FAIRE USER"""
                questions = [
                    {
                        "type": "list",
                        "message": "Que voulez-vous faire?",
                        "choices": [
                            "Rechercher des jeux",
                            "Afficher votre wishlist",
                            "Ajouter un jeu à votre wishlist",
                            "Supprimer un jeu de votre wishlist",
                            "Ajouter une note à un jeu",
                            "Ajouter un pouce bleu à un jeu",
                            "Ajouter un pouce rouge à un jeu",
                            "Enlever un" " pouce bleu à un jeu",
                            "Enlever un pouce rouge à un jeu",
                            "Afficher le pourcentage de pouces bleu",
                            "Se deconnecter",
                        ],
                    },
                ]

                result = prompt(questions)
                choix = result[0]

                if choix == "Rechercher des jeux":
                    """BLOC RECHERCHER JEU USER"""
                    question = [
                        {
                            "type": "list",
                            "message": "Que recherchez-vous ?",
                            "choices": [
                                "Identifiant",
                                "Nom",
                                "Date de sortie",
                                "Genre",
                                "Prix",
                                "Developpeur",
                                "Description",
                                "Nombre de pouces bleu",
                                "Nombre de pouces rouge",
                                "Note (entre 0 et 100)",
                                "Nombre de DLC",
                                "Jeux disponibles sur Windows",
                                "Jeux disponibles sur Mac",
                                "Jeux disponibles sur Linux",
                                "temps de jeu moyen",
                                "temps de jeu moyen 2 semaines après"
                                " la sortie",
                                "Age limite",
                                "rien :)",
                            ],
                        },
                    ]

                    result = prompt(question)
                    attribut = result[0]
                    if attribut == "Prix":
                        question = [
                            {
                                "type": "number",
                                "message": "Quel est le prix minimum?",
                                "name": "prixmin",
                            },
                            {
                                "type": "number",
                                "message": "Quel est le prix maximum?.",
                                "name": "prixmax",
                            },
                        ]
                        result = prompt(question)
                        valeurattrib = (result["prixmin"], result["prixmax"])
                        recherche = Recherche(attribut, valeurattrib)
                        recherche.rechercher()
                    elif attribut == "Note (entre 0 et 100)":
                        question = [
                            {
                                "type": "number",
                                "message": "Quel est la note minimale?",
                                "name": "notemin",
                            },
                            {
                                "type": "number",
                                "message": "Quel est la note maximale?",
                                "name": "notemax",
                            },
                        ]
                        result = prompt(question)
                        valeurattrib = (result["notemin"], result["notemax"])
                        recherche = Recherche(attribut, valeurattrib)
                        recherche.rechercher()
                    else:
                        question = [
                            {
                                "type": "input",
                                "message": f"Precisez {attribut} :",
                                "name": "valeurattrib",
                            }
                        ]
                        result = prompt(question)
                        valeurattrib = result["valeurattrib"]

                        recherche = Recherche(attribut, valeurattrib)
                        recherche.rechercher()

                elif choix == "Afficher votre wishlist":
                    """BLOC AFFICHER WISHLIST USER"""
                    user.afficher_wishlist()

                elif choix == "Ajouter un jeu à votre wishlist":
                    """BLOC AJOUTER UN JEU WISHLIST USER"""
                    questions = [
                        {
                            "type": "input",
                            "message": "Quel est le nom du jeu"
                            " que vous voulez ajouter ?",
                            "name": "nom",
                        }
                    ]
                    result = prompt(questions)
                    user.ajouter_wishlist(result["nom"])

                elif choix == "Supprimer un jeu de votre wishlist":
                    """BLOC SUPPRIMER UN JEU WISHLIST USER"""
                    questions = [
                        {
                            "type": "input",
                            "message": "Quel est le nom du jeu"
                            " que vous voulez supprimer ?",
                            "name": "nom",
                        }
                    ]

                    result = prompt(questions)
                    user.supprimer_wishlist(result["nom"])

                elif choix == "Ajouter une note à un jeu":
                    """BLOC AJOUTER UNE NOTE A UN JEU USER"""
                    questions = [
                        {
                            "type": "number",
                            "message": "Que est l'identifiant du jeu?",
                            "name": "id",
                        },
                        {
                            "type": "number",
                            "message": "Quelle note donnez-vous à ce jeu?"
                            " (Entier entre 0 et 100)",
                            "name": "note",
                        },
                    ]
                    result = prompt(questions)
                    id = result["id"]
                    note = result["note"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.ajouter_note(int(note), numligneuser)

                elif choix == "Ajouter un pouce bleu à un jeu":
                    """BLOC AJOUTER UN POUCE BLEU A UN JEU USER"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.ajoute_poucebleu(numligneuser)

                elif choix == "Ajouter un pouce rouge à un jeu":
                    """BLOC AJOUTER UN POUCE ROUGE A UN JEU USER"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.ajoute_poucerouge(numligneuser)

                elif choix == "Enlever un pouce bleu à un jeu":
                    """BLOC ENLEVER UN POUCE BLEU A UN JEU USER"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.supprime_poucebleu(numligneuser)

                elif choix == "Enlever un pouce rouge à un jeu":
                    """BLOC ENLEVER UN POUCE ROUGE A UN JEU USER"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.supprime_poucerouge(numligneuser)

                elif choix == "Afficher le pourcentage de pouces bleu":
                    """BLOC AFFICHER LE POURCENTAGE DE POUCES BLEU USER"""
                    questions = {
                        "type": "number",
                        "message": "Que est l'identifiant du jeu?",
                        "name": "id",
                    }
                    result = prompt(questions)
                    id = result["id"]
                    with open("ressources/jeuxclean.csv", "r") as file:
                        reader = csv.reader(file)
                        reader = list(reader)
                        for row in reader:
                            if row[0] == str(id):
                                jeu = Jeux(
                                    int(row[0]),
                                    row[1],
                                    row[2],
                                    row[3],
                                    float(row[4]),
                                    row[5],
                                    row[6],
                                    int(row[7]),
                                    int(row[8]),
                                    float(row[9]),
                                    int(row[10]),
                                    bool(row[11]),
                                    bool(row[12]),
                                    bool(row[13]),
                                    float(row[14]),
                                    float(row[15]),
                                    int(row[16]),
                                )
                    jeu.recommendation_pouce()

                elif choix == "Se deconnecter":
                    """BLOC SE DECONNECTER"""
                    user.deconnexion()

    elif choix == "Créer un compte":
        """BLOC CREER UN COMPTE"""
        questions = [
            {"type": "input", "message": "Choisissez un pseudo.",
             "name": "pseudo"},
            {"type": "number", "message": "Quel est votre âge?",
             "name": "age"},
        ]

        result = prompt(questions)
        pseudo = result["pseudo"]
        age = result["age"]
        Invite().creer_compte(pseudo, age)

    elif choix == "Rechercher des jeux":
        """BLOC RECHERCHER DES JEUX INVITE"""
        rech = 1
        while rech != 0:
            question = [
                {
                    "type": "list",
                    "message": "Que recherchez-vous ?",
                    "choices": [
                        "Identifiant",
                        "Nom",
                        "Date de sortie",
                        "Genre",
                        "Prix",
                        "Developpeur",
                        "Description",
                        "Nombre de pouces bleu",
                        "Nombre de pouces rouge",
                        "Note (entre 0 et 100)",
                        "Nombre de DLC",
                        "Jeux disponibles sur Windows",
                        "Jeux disponibles sur Mac",
                        "Jeux disponibles sur Linux",
                        "temps de jeu moyen",
                        "temps de jeu moyen 2 semaines après" " la sortie",
                        "Age limite",
                        "Arrêter la recherche",
                    ],
                },
            ]
            result = prompt(question)
            attribut = result[0]
            if attribut == "Arrêter la recherche":
                rech = 0
            elif attribut == "Prix":
                question = [
                    {
                        "type": "number",
                        "message": "Quel est le prix minimum?",
                        "name": "prixmin",
                    },
                    {
                        "type": "number",
                        "message": "Quel est le prix maximum?",
                        "name": "prixmax",
                    },
                ]
                result = prompt(question)
                valeurattrib = (result["prixmin"], result["prixmax"])
                recherche = Recherche(attribut, valeurattrib)
                recherche.rechercher()
            elif attribut == "Note (entre 0 et 100)":
                question = [
                    {
                        "type": "number",
                        "message": "Quel est la note minimale?",
                        "name": "notemin",
                    },
                    {
                        "type": "number",
                        "message": "Quel est la note maximale?",
                        "name": "notemax",
                    },
                ]
                result = prompt(question)
                valeurattrib = (result["notemin"], result["notemax"])
                recherche = Recherche(attribut, valeurattrib)
                recherche.rechercher()
            else:
                question = [
                    {
                        "type": "input",
                        "message": f"Precisez {attribut} :",
                        "name": "valeurattrib",
                    }
                ]
                result = prompt(question)
                valeurattrib = result["valeurattrib"]

                recherche = Recherche(attribut, valeurattrib)
                recherche.rechercher()
    else:
        """BLOC QUITTER STIM"""
        actif = 0
        print("Fermeture de l'application. A bientôt sur Stim!")

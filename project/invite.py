import csv


class Invite:
    """Invité est le statut par défaut lorsque l'on arrive sur Stim.
    Les invités peuvent se connecter, créer un compte, faire des recherches
    ou quitter l'application."""

    def creer_compte(self, pseudo, age):
        """creer_compte permet aux utilisateurs de créer un compte Stim
        pour avoir accès à d'autres fonctionnalitées.
        creer_compte ajoute une ligne dans le fichier csv des utilisateurs
        avec leur identifiant, pseudo, age, wishlist et type d'utilisateur
        (forcément user.

        Parameters
        ----------
        pseudo : str
        Pseudo du nouveau compte.

        age:
        Age de l'utilisateur.
        """
        identifiants = set()
        with open("ressources/bddutilisateurs.csv", "r", newline="") as file:
            dataset = csv.reader(file)
            dataset = list(dataset)
            for ligne in dataset:
                identifiants.add(ligne[0])
            a = max(identifiants)
            newid = int(a) + 1
        nouvelutilisateur = [newid, pseudo, age, "'[]'", "user", "'[]'",
                             "'[]'", "'[]'"]
        with open("ressources/bddutilisateurs.csv", mode="a",
                  newline="") as bdd:
            print("Création du compte en cours, veuillez patientier.")
            writer = csv.writer(bdd)
            writer.writerow(nouvelutilisateur)
        print(
            f"Félicitations, le compte à bien été créé!\nBienvenue {pseudo}!"
            f" Votre identifiant est {newid}."
        )

    def connexion(self, id):
        """Permet à un invité de se connecter et de passer user ou admin.
        connexion vérifie dans le fichier csv des utilisateurs si leur compte
        existe bien.

        Parameters
        ----------
        id : int
        Identifiant du compte de l'utilisateur.
        """
        if not isinstance(id, int):
            raise TypeError("L'identifiant doit être un entier.")
        with open("ressources/bddutilisateurs.csv", "r") as file:
            reader = csv.reader(file)
            reader = list(reader)
            for row in reader:
                if int(row[0]) == id:
                    print(f"Connexion effectuée. Bienvenue {row[1]}!")
                    return row[4]
            print("Erreur de connexion. Cet identifiant n'existe pas :/")

    def numligneuser(self, id):
        """Permet d'avoir accès à la ligne du fichier csv des utilisateurs
        corrspondant à l'utilisateur possédant l'identifiant id

        Parameters
        ----------
        id : int
        Identifiant de l'utilisateur dont on veut connaitre le
        numéro de la ligne.
        """
        with open("ressources/bddutilisateurs.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            for i in range(len(rows)):
                if int(rows[i][0]) == id:
                    return i
            raise ValueError("Cet utilisateur n'existe pas.")

import csv

from project.jeux import Jeux
from project.utilisateurConnecte import Utilisateurconnecte


class Admin(Utilisateurconnecte):
    """Les administrateurs  s'occupent de la gestion et l'administration de la
    plateforme. Cela inclut la gestion des jeux disponibles,
    des commentaires et Sdes évaluations, ainsi que la modération du contenu.
    """

    def __init__(
        self,
        userid,
        pseudo,
        age,
        wishlist,
        listenote,
        listepoucebleu,
        listepoucerouge,
        st=1,
    ):
        super().__init__(
            userid,
            pseudo,
            age,
            wishlist,
            listenote,
            listepoucebleu,
            listepoucerouge,
            st=1,
        )

    def ajouter_jeux(
        self,
        nom,
        date,
        genre,
        prix,
        developpeur,
        description,
        nombre_dlc,
        windows,
        mac,
        linux,
        age_limite,
        temps_moyen=0,
        temps_moyen_2semaine=0,
        poucebleu=0,
        poucerouge=0,
        note=0,
    ):
        """Permet d'ajouter un jeu au catalogue.
            ajouter_jeux ajoute une ligne dans le fichier csv des jeux
            Parameters
            ----------
            nom : str
            Nom du jeu.

            date : str
            Date de sortie du jeu.
            genre : str
            Genre du jeu.

                ajouter_jeux ajoute une ligne dans le fichier csv des jeux
                Parameters
                ----------
                nom : str
                Nom du jeu.

                date : str
                Date de sortie du jeu.


                genre : str
                Genre du jeu.

                prix : float
                Prix du jeu.

                developpeur : str
                Developpeur du jeu.

                description: str
                Description du jeu.

                poucebleu : int
                Nombre de pouces bleu.

                poucerouge : int
                Nombre de pouces rouge.

                note : list
                Liste des notes du jeu.

                nombre_dlc : int
                Nombre de DLC.

                windows : bool
                Booléen qui indique si le jeu est disponible sous Windows.

                Mac : bool
                Booléen qui indique si le jeu est disponible sous Mac.

                Linux : bool
                Booléen qui indique si le jeu est disponible sous Linux.

                temps_moyen : float
                Temps de jeu moyen depuis la sortie du jeu.

                temps_moyen_2semaine : float
                Temps de jeu moyen deux semaines après la sortie du jeu.

                age_limite : int
                Age minimal conseillé pour jouer.
        """

        if isinstance(nom, str) is False:
            raise TypeError("Le nom doit être une chaîne de caractère.")
        elif isinstance(date, str) is False:
            raise TypeError("La date doit être une chaîne de caractère.")
        elif isinstance(genre, str) is False:
            raise TypeError("Le genre doit être une chaîne de caractère.")
        elif isinstance(prix, float) is False:
            raise TypeError("Le prix doit être un réel.")
        elif isinstance(developpeur, str) is False:
            raise TypeError(
                "Le nom du développeur doit être une chaîne de"
                " caractère."
            )
        elif isinstance(description, str) is False:
            raise TypeError("La description doit être une chaîne de"
                            " caractère.")
        elif isinstance(poucebleu, int) is False:
            raise TypeError("Poucebleu doit être un entier.")
        elif poucebleu < 0:
            raise ValueError("Poucebleu doit être positif.")
        elif isinstance(poucerouge, int) is False:
            raise TypeError("Poucerouge doit être un entier.")
        elif poucerouge < 0:
            raise ValueError("Poucerouge doit être positif.")
        if isinstance(note, float) is False:
            raise TypeError("Note doit être un flottant.")
        if isinstance(nombre_dlc, int) is False:
            raise TypeError("Le nombre de DLC doit être un entier.")
        elif isinstance(windows, bool) is False:
            raise TypeError("Windows doit être un booléen.")
        elif isinstance(mac, bool) is False:
            raise TypeError("Mac doit être un booléen.")
        elif isinstance(linux, bool) is False:
            raise TypeError("Linux doit être un booléen.")
        if isinstance(temps_moyen, float) is False:
            raise TypeError("Le temps de jeu moyen doit être un flottant.")
        elif temps_moyen < 0:
            raise ValueError("Le temps de jeu moyen doît être positif.")
        elif isinstance(temps_moyen_2semaine, float) is False:
            raise TypeError(
                "Le temps de jeu moyen au bout de 2 semaines doit"
                " être un flottant."
            )
        elif temps_moyen_2semaine < 0:
            raise ValueError(
                "Le temps de jeu moyen au bout de 2 semaines doit"
                " être positif."
            )
        elif isinstance(age_limite, int) is False:
            raise TypeError("L'âge limite doit être un entier.")
        elif age_limite < 0:
            raise ValueError("L'âge limite doit être positif.")

        identifiants = set()
        with open("ressources/jeuxclean.csv", "r", newline="") as file:
            dataset = csv.reader(file)
            dataset = list(dataset)
            for ligne in dataset:
                identifiants.add(ligne[0])
            a = max(identifiants)
            newid = int(a) + 1

        Jeux(
            newid,
            nom,
            date,
            genre,
            prix,
            developpeur,
            description,
            poucebleu,
            poucerouge,
            note,
            nombre_dlc,
            windows,
            mac,
            linux,
            temps_moyen,
            temps_moyen_2semaine,
            age_limite,
        )

        nouveau_jeu = [
            newid,
            nom,
            date,
            genre,
            prix,
            developpeur,
            description,
            poucebleu,
            poucerouge,
            note,
            nombre_dlc,
            windows,
            mac,
            linux,
            temps_moyen,
            temps_moyen_2semaine,
            age_limite,
        ]

        with open("ressources/jeuxclean.csv", mode="a", newline="") as games:
            print("Ajout du jeu en cours. veuillez patientier.")
            writer = csv.writer(games)
            writer.writerow(nouveau_jeu)
        print("Le jeu a été ajouté avec succès!")

    def supprimer_jeux(self, id):
        """Supprimer un jeu du csv.
        Permet de supprimer un jeu dun fichier csv en fonction de
        l'identifiant. Renvoi un erreur si l'identifiant n'est pas dans le csv.

        Parameters
        ----------
        id : int
        Identifiant du jeu à supprimer.
        """
        if isinstance(id, int) is False:
            raise TypeError("L'identifiant doit être un entier.")
        elif id < 0:
            raise ValueError("L'identifiant doit être positif.")
        with open("ressources/jeuxclean.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            identifiants = set()
            for ligne in rows:
                identifiants.add(int(ligne[0]))
                print(int(ligne[0]))
        if id in identifiants is False:
            raise ValueError("Aucun jeu ne possède cet identifiant.")
        filtered_rows = [row for row in rows if row and row[0] != str(id)]

        with open("ressources/jeuxclean.csv", "w", newline="") as outfile:
            print("Suppression du jeu en cours. Veuillez patienter.")
            writer = csv.writer(outfile)
            writer.writerows(filtered_rows)

        print("Le jeu a été supprimé avec succès!")

    def modifier_jeux(self, id, attribut, nouvelattrib):
        """Description courte sur une ligne.
        Permet de modifier l'attribut du jeu.
        Modifier jeu modfie le csv contenant toutes les informations sur
        les jeux.
        Prend en argument l'identifiant du jeu à modifier, l'attribut à
        modifier et la nouvelle valeur de l'attribut.
        Parameters
        ----------
        id : int
        identifiant du jeu à modifier
        attribut : str
        attribut à modifier
        nouvelattrib :
        Valeur du nouvel attribut. Le type varie en fcontion de l'attribut
        choisi.
        """
        attrib = {
            "nom": 1,
            "date": 2,
            "genre": 3,
            "prix": 4,
            "developpeur": 5,
            "description": 6,
            "poucebleu": 7,
            "poucerouge": 8,
            "note": 9,
            "nombre_dlc": 10,
            "windows": 11,
            "mac": 12,
            "linux": 13,
            "temps_moyen": 14,
            "temps_moyen_2semaine": 15,
            "age_limite": 16,
        }
        if isinstance(attribut, str) is False:
            raise TypeError(
                "L'attribut à modifier doit être une chaîne de caractère."
            )
        if attribut in attrib is False:
            raise ValueError("Cet attribut n'existe pas.")
        if attribut == "nom" and isinstance(nouvelattrib, str) is False:
            raise TypeError("Le nouveau nom doit être"
                            " une chaîne de caractères.")
        elif attribut == "date" and isinstance(nouvelattrib, str) is False:
            raise TypeError("La nouvelle date doit être"
                            " une chaîne de caractères.")
        elif attribut == "genre" and isinstance(nouvelattrib, str) is False:
            raise TypeError("Le nouveau genre doit être"
                            " une chaîne de caractères.")
        elif attribut == "prix" and isinstance(nouvelattrib, float) is False:
            raise TypeError("Le nouveau prix doit être un flottant.")
        elif attribut == "developpeur" and isinstance(nouvelattrib,
                                                      str) is False:
            raise TypeError(
                "Le nouveau developpeur doit être" " une chaîne de caractères."
            )
        elif attribut == "description" and isinstance(nouvelattrib,
                                                      str) is False:
            raise TypeError(
                "La nouvelle description doit être"
                " une chaîne de caractères."
            )
        elif attribut == "poucebleu" and isinstance(nouvelattrib,
                                                    int) is False:
            raise TypeError("Le nouveau nombre de pouces bleu doit"
                            " être un entier.")
        elif attribut == "poucerouge" and isinstance(nouvelattrib,
                                                     int) is False:
            raise TypeError("Le nouveau nombre de pouces rouge doit"
                            " être un entier.")
        elif attribut == "note" and isinstance(nouvelattrib, float) is False:
            raise TypeError("La nouvelle note doit être"
                            " un flottant.")
        elif attribut == "nombre_dlc" and isinstance(nouvelattrib,
                                                     int) is False:
            raise TypeError("Le nouveau nombre de DLC doit être" " un entier.")
        elif attribut == "windows" and isinstance(nouvelattrib, bool) is False:
            raise TypeError("La nouvelle valeur de windows doit être "
                            "un booléen.")
        elif attribut == "mac" and isinstance(nouvelattrib, bool) is False:
            raise TypeError("La nouvelle valeur de mac doit être "
                            "un booléen.")
        elif attribut == "linux" and isinstance(nouvelattrib, bool) is False:
            raise TypeError("La nouvelle valeur de linux doit être "
                            "un booléen.")
        elif attribut == "temps_moyen" and isinstance(nouvelattrib,
                                                      float) is False:
            raise TypeError("Le nouveau temps moyen doit être "
                            " un flottant.")
        elif (
            attribut == "temps_moyen_2semaine"
            and isinstance(nouvelattrib, float) is False
        ):
            raise TypeError(
                "Le nouveau temps moyen après deux semaines"
                " doit être un flottant."
            )
        elif attribut == "age" and isinstance(nouvelattrib, int) is False:
            raise TypeError("Le nouvel age limite doit être un entier.")

        with open("ressources/jeuxclean.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            c = 0
            for i in range(len(rows)):
                if int(rows[i][0]) == id:
                    numligne = i
                    c += 1
                    break
            if c == 0:
                raise ValueError("Aucun jeu ne possède cet identifiant")

        rows[numligne][attrib[attribut]] = nouvelattrib

        with open("ressources/jeuxclean.csv", "w", newline="") as csvfile:
            print("Modification en cours, veuillez patienter.")
            writer = csv.writer(csvfile)
            writer.writerows(rows)

        print("Le jeu a été modifié avec succès!")

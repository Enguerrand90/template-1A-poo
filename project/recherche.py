import csv

from project.jeux import Jeux


class Recherche:
    """Permet à un utilisateur de rechercher
    selon un critère de selection proposé.
    L'utilisateur peut filtrer selon ;'attrivut qu'il souhaite

    parameters
    ----------
    attribut : int
    Attribut du ju par lequel l'utilisateur veut filtrer

    valeurattrib :
    Valeur de l'attribut en question. Le type dépend de l'attribut choisi.
    """

    def __init__(self, attribut, valeurattrib):
        if isinstance(attribut, str) is False:
            raise TypeError("L'attribut doit être une chaîne de caractères.")
        self.attribut = attribut
        self.valeurattrib = valeurattrib

    def rechercher(self):
        """fonction renvoyant les jeux correspondant
        à une demande spécifiée par l'utilisateur,
        du fichier csv.

        rechercher() -> List(Jeux)
        """
        attrib = {
            "Identifiant": 0,
            "Nom": 1,
            "Date de sortie": 2,
            "Genre": 3,
            "Prix": 4,
            "Developpeur": 5,
            "Description": 6,
            "Nombre de pouces bleu": 7,
            "Nombre de pouces rouge": 8,
            "Note (entre 0 et 100)": 9,
            "Nombre de DLC": 10,
            "Jeux disponibles sur Windows": 11,
            "Jeux disponibles sur Mac": 12,
            "Jeux disponibles sur Linux": 13,
            "Temps de jeu moyen": 14,
            "Temps de jeu moyen 2 semaines après": 15,
            "Age limite": 16,
        }

        col = attrib[self.attribut]

        if self.attribut == "Prix":
            prixmin = self.valeurattrib[0]
            prixmax = self.valeurattrib[1]
            if prixmin > prixmax:
                raise ValueError(
                    "Le prix minimum doit être inférieur au" " prix maximum."
                )
            with open("ressources/jeuxclean.csv", "r") as file:
                print("Recherche des jeux en cours. Veuilez patienter.")
                reader = csv.reader(file)
                reader = list(reader)
                L = []
                for row in reader:
                    if row[col] >= str(prixmin) and row[col] <= str(prixmax):
                        L.append(
                            Jeux(
                                int(row[0]),
                                row[1],
                                row[2],
                                row[3],
                                float(row[4]),
                                row[5],
                                row[6],
                                int(row[7]),
                                int(row[8]),
                                float(row[9]),
                                int(row[10]),
                                bool(row[11]),
                                bool(row[12]),
                                bool(row[13]),
                                float(row[14]),
                                float(row[15]),
                                int(row[16]),
                            )
                        )
                if L == []:
                    print("Aucun jeu sur Stim ne correspond à vos critères :/")
                print(L)
        elif self.attribut == "Note (entre 0 et 100)":
            notemin = self.valeurattrib[0]
            notemax = self.valeurattrib[1]
            if notemin > notemax:
                raise ValueError(
                    "Le prix minimum doit être inférieur au" " prix maximum."
                )
            with open("ressources/jeuxclean.csv", "r") as file:
                print("Recherche des jeux en cours. Veuilez patienter.")
                reader = csv.reader(file)
                reader = list(reader)
                L = []
                for row in reader:
                    if row[col] >= str(notemin) and row[col] <= str(notemax):
                        L.append(
                            Jeux(
                                int(row[0]),
                                row[1],
                                row[2],
                                row[3],
                                float(row[4]),
                                row[5],
                                row[6],
                                int(row[7]),
                                int(row[8]),
                                float(row[9]),
                                int(row[10]),
                                bool(row[11]),
                                bool(row[12]),
                                bool(row[13]),
                                float(row[14]),
                                float(row[15]),
                                int(row[16]),
                            )
                        )
                if L == []:
                    print("Aucun jeu sur Stim ne correspond à vos critères :/")
                print(L)

        else:
            with open("ressources/jeuxclean.csv", "r") as file:
                print("Recherche des jeux en cours. Veuilez patienter.")
                reader = csv.reader(file)
                reader = list(reader)
                L = []
                for row in reader:
                    if row[col] == str(self.valeurattrib):
                        L.append(
                            Jeux(
                                int(row[0]),
                                row[1],
                                row[2],
                                row[3],
                                float(row[4]),
                                row[5],
                                row[6],
                                int(row[7]),
                                int(row[8]),
                                float(row[9]),
                                int(row[10]),
                                bool(row[11]),
                                bool(row[12]),
                                bool(row[13]),
                                float(row[14]),
                                float(row[15]),
                                int(row[16]),
                            )
                        )
                if L == []:
                    print("Aucun jeu sur Stim ne correspond à vos critères :/")
                print(L)

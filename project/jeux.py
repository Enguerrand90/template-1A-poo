import csv


def convertisseur_liste(liste):
    """Convertit une wishlist en une autre wishlist avec le bon format.

    Returns:
    ----------
        list: La wishlist avec le bon format.
    """
    resultat = []
    mot = ""
    for caractere in liste:
        if caractere == ",":
            resultat.append(mot.strip())
            mot = ""
        else:
            mot += caractere

    resultat.append(mot.strip())
    return resultat


def supcrochetliste(liste):
    n = len(liste)
    if liste == []:
        return liste
    elif len(liste) == 1:
        k = len(liste)
        liste[0] = liste[0][0: k - 1]
    k = len(liste[n - 1])
    liste[0] = liste[0][1:]
    liste[n - 1] = liste[n - 1][: k - 1]
    return liste


def supguillemetliste(liste):
    n = len(liste)
    for i in range(n):
        k = len(liste[i])
        liste[i] = liste[i][1: k - 1]
    return liste


class Jeux:
    """CLasse modélisant un jeu.

    Parameters
    ----------
    identifiant : str
        identifiant du jeu.

    nom : str
        nom du jeu.

    date : date
        date de sortie du jeu.

    genre : str
        genre du jeu.

    prix : float
        prix actuel du jeu sur la plateforme.

    developpeur : str
        nom du développeur du jeu.

    description_du_jeu : str
        description du jeu.

    poucebleu : int
        Nombre de pouces bleu donnés par les utilisateurs.

    poucerouge : int
        Nombre de pouces rouge donnés par les utilisateurs.

    note : list[int]
        liste des notes données par les utilisateurs.

    nombre_dlc : int
        nombre actuel de DLC sur le jeu.

    windows : bool
        indique si le jeu est disponible sous Windows.

    mac : bool
        indique si le jeu est disponible sous Mac.

    linux : bool
        indique si le jeu est disponible sous Linux.

    temps_jeu_moyen : float
        moyenne du temps passé sur le jeu pour un utilisateur.

    temps_jeu_moyen_apres : float
        moyenne du temps passé sur le jeu pour un utilisateur
        deux semaines après la sortie.

    age_limite : int
        age limite pour le joueur.
    """

    def __init__(
        self,
        identifiant,
        nom,
        date,
        genre,
        prix,
        developpeur,
        description,
        poucebleu,
        poucerouge,
        note,
        nombre_dlc,
        windows,
        mac,
        linux,
        temps_moyen,
        temps_moyen_2semaines,
        age_limite,
    ):
        if not isinstance(identifiant, int):
            raise TypeError("L'identifiant doit être un entier.")
        if not isinstance(nom, str):
            raise TypeError("Le nom doit être une chaîne de caractères.")
        if not isinstance(date, str):
            raise TypeError("La date doit être une chaine de caractères.")
        if not isinstance(genre, str):
            raise TypeError("Le genre doit être une chaîne de caractères.")
        if not isinstance(prix, float):
            raise TypeError("Le prix doit être un nombre flottant.")
        if not isinstance(developpeur, str):
            raise TypeError("Le développeur doit être une chaîne de"
                            " caractères.")
        if not isinstance(description, str):
            raise TypeError("La description  doit être une chaîne de"
                            " caractères.")
        if not isinstance(poucebleu, int):
            raise TypeError("Poucebleu doit être un entier.")
        if not isinstance(poucerouge, int):
            raise TypeError("Poucerouge doit être un entier.")
        if not isinstance(note, float):
            raise TypeError("Note doit être un flottant.")

        if note > 100:
            raise ValueError("Les notes doivent être inférieures à 100.")
        if not isinstance(nombre_dlc, int):
            raise TypeError("Le nombre de DLC doit être un entier.")
        if not isinstance(windows, bool):
            raise TypeError("Windows doit être un booléen.")
        if not isinstance(mac, bool):
            raise TypeError("Mac doit être un booléen.")
        if not isinstance(linux, bool):
            raise TypeError("Linux doit être un booléen.")
        if not isinstance(temps_moyen, float):
            raise TypeError("Le temps de jeu moyen doit être"
                            " un nombre flottant.")
        if not isinstance(temps_moyen_2semaines, float):
            raise TypeError(
                "Le temps de jeu moyen après la sortie doit être"
                " un nombre flottant."
            )
        if not isinstance(age_limite, int):
            raise TypeError("L'âge limite doit être un entier.")
        self.identifiant = identifiant
        self.nom = nom
        self.date = date
        self.genre = genre
        self.prix = prix
        self.developpeur = developpeur
        self.description_du_jeu = description
        self.poucebleu = poucebleu
        self.poucerouge = poucerouge
        self.note = note
        self.nombre_dlc = nombre_dlc
        self.windows = windows
        self.mac = mac
        self.linux = linux
        self.temps_moyen = temps_moyen
        self.temps_moyen_2semaines = temps_moyen_2semaines
        self.age_limite = age_limite

    def __repr__(self):
        """Représenation d'un jeu."""
        return (
            f"\n--->{self.nom}:\n"
            f"Identifiant: '{self.identifiant}'\n"
            f"Date de sortie: '{self.date}'\n"
            f"Genre: '{self.genre}'\n"
            f"Prix: '{self.prix}',\n"
            f"Developpeur: '{self.developpeur}'\n"
            f"Description: '{self.description_du_jeu}'\n"
            f"Nombre de pouces bleu: '{self.poucebleu}'\n"
            f"Nombre de pouces rouge: '{self.poucerouge}'\n"
            f"Note: '{self.note}'/100\n"
            f"Nombre de DLC: '{self.nombre_dlc}'\n"
            f"Disponible sur Windows:'{self.windows}'\n"
            f"Disponible sur Mac:'{self.windows}'\n"
            f"Disponible sur Lindows:'{self.linux}'\n"
            f"Temps de jeu moyen: '{self.temps_moyen}' heures\n"
            f"Temps de jeu moyen deux semaines"
            f" après la sortie:'{self.temps_moyen_2semaines} heures'\n"
            f"Age limite: '{self.age_limite}')"
        )

    def ajouter_note(self, nouvellenote, numligneuser):
        """Méthode calculant la nouvelle note finale
        Parameters
        ----------
        nouvellenote : int
        Nouvelle note entre 0 et 100

        """
        if isinstance(nouvellenote, int) is False:
            raise TypeError("La nouvelle note doit être un entier.")
        elif nouvellenote < 0 or nouvellenote > 100:
            raise ValueError("La note doît être comprise entre 0 et 100.")
        with open("ressources/bddutilisateurs.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            rows[numligneuser][5] = list(rows[numligneuser][5])
            rows[numligneuser][5] = convertisseur_liste(
                                    rows[numligneuser][5])
            rows[numligneuser][5] = supcrochetliste(rows[numligneuser][5])
            rows[numligneuser][5] = supguillemetliste(
                                    rows[numligneuser][5])
            if str(self.identifiant) in rows[numligneuser][5]:
                raise ValueError(f"Vous avez déjà noté {self.nom}.")
            else:
                rows[numligneuser][5].append(str(self.identifiant))
                with open("ressources/bddutilisateurs.csv", "w",
                          newline="") as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerows(rows)

        if self.poucebleu == 0 and self.poucerouge == 0:
            self.note = (self.note + nouvellenote) / 2
        else:
            coef = self.poucebleu + self.poucerouge - 1
            self.note = (coef * self.note + nouvellenote) / (
                self.poucebleu + self.poucerouge
            )

        with open("ressources/jeuxclean.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            for i in range(len(rows)):
                if int(rows[i][0]) == self.identifiant:
                    numligne = i
                    break
        rows[numligne][9] = round(self.note, 2)

        with open("ressources/jeuxclean.csv", "w", newline="") as csvfile:
            print("Ajout de la note en cours, veuillez patienter.")
            writer = csv.writer(csvfile)
            writer.writerows(rows)
            print("La note a été ajoutée avec succès!")

    def ajoute_poucebleu(self, numligneuser):
        """Ajoute un pouce bleu au jeu."""

        with open("ressources/bddutilisateurs.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            rows[numligneuser][6] = list(rows[numligneuser][6])
            rows[numligneuser][6] = convertisseur_liste(
                                    rows[numligneuser][6])
            rows[numligneuser][6] = supcrochetliste(rows[numligneuser][6])
            rows[numligneuser][6] = supguillemetliste(
                                    rows[numligneuser][6])

            rows[numligneuser][7] = list(rows[numligneuser][7])
            rows[numligneuser][7] = convertisseur_liste(
                                    rows[numligneuser][7])
            rows[numligneuser][7] = supcrochetliste(rows[numligneuser][7])
            rows[numligneuser][7] = supguillemetliste(
                                    rows[numligneuser][7])
            if str(self.identifiant) in rows[numligneuser][6]:
                raise ValueError("Vous avez déjà donné"
                                 f" un pouce bleu à {self.nom}.")
            elif str(self.identifiant) in rows[numligneuser][7]:
                raise ValueError("vous avez déjà donné"
                                 f" un pouce rouge à {self.nom}")
            else:
                rows[numligneuser][6].append(str(self.identifiant))
                with open("ressources/bddutilisateurs.csv", "w",
                          newline="") as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerows(rows)

        self.poucebleu += 1

        with open("ressources/jeuxclean.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            for i in range(len(rows)):
                if int(rows[i][0]) == int(self.identifiant):
                    numligne = i
                    break
            rows[numligne][7] = self.poucebleu

        with open("ressources/jeuxclean.csv", "w", newline="") as csvfile:
            print("Ajout du pouce bleu en cours, veuillez patienter.")
            writer = csv.writer(csvfile)
            writer.writerows(rows)
            print("Le pouce bleu a été ajouté avec succès!")

    def ajoute_poucerouge(self, numligneuser):
        """Ajoute un pouce rouge au jeu."""
        with open("ressources/bddutilisateurs.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            rows[numligneuser][6] = list(rows[numligneuser][6])
            rows[numligneuser][6] = convertisseur_liste(
                                    rows[numligneuser][6])
            rows[numligneuser][6] = supcrochetliste(rows[numligneuser][6])
            rows[numligneuser][6] = supguillemetliste(
                                    rows[numligneuser][6])

            rows[numligneuser][7] = list(rows[numligneuser][7])
            rows[numligneuser][7] = convertisseur_liste(
                                    rows[numligneuser][7])
            rows[numligneuser][7] = supcrochetliste(rows[numligneuser][7])
            rows[numligneuser][7] = supguillemetliste(
                                    rows[numligneuser][7])

            if str(self.identifiant) in rows[numligneuser][6]:
                raise ValueError(f"Vous avez déjà donné un"
                                 f" pouce bleu à {self.nom}.")
            elif str(self.identifiant) in rows[numligneuser][7]:
                raise ValueError("vous avez déjà donné un pouce"
                                 f" rouge à {self.nom}")
            else:
                rows[numligneuser][7].append(str(self.identifiant))
                with open("ressources/bddutilisateurs.csv", "w",
                          newline="") as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerows(rows)

        self.poucerouge += 1

        with open("ressources/jeuxclean.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            for i in range(len(rows)):
                if int(rows[i][0]) == int(self.identifiant):
                    numligne = i
                    break
            rows[numligne][8] = self.poucerouge

        with open("ressources/jeuxclean.csv", "w", newline="") as csvfile:
            print("Ajout du pouce rouge en cours, veuillez patienter.")
            writer = csv.writer(csvfile)
            writer.writerows(rows)
        print("Le pouce rouge a été jouté avec succès!")

    def supprime_poucebleu(self, numligneuser):
        """Enlève un pouce bleu au jeu."""

        with open("ressources/bddutilisateurs.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            rows[numligneuser][6] = list(rows[numligneuser][6])
            rows[numligneuser][6] = convertisseur_liste(
                                    rows[numligneuser][6])
            rows[numligneuser][6] = supcrochetliste(rows[numligneuser][6])
            rows[numligneuser][6] = supguillemetliste(
                                    rows[numligneuser][6])

            if str(self.identifiant) not in rows[numligneuser][6]:
                raise ValueError(
                    f"Vous n'avez pas encore donné un pouce bleu à {self.nom}."
                )

            else:
                rows[numligneuser][6].remove(str(self.identifiant))
                with open("ressources/bddutilisateurs.csv", "w",
                          newline="") as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerows(rows)

        self.poucebleu -= 1

        with open("ressources/jeuxclean.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            for i in range(len(rows)):
                if int(rows[i][0]) == self.identifiant:
                    numligne = i
                    break
        rows[numligne][7] = self.poucebleu

        with open("ressources/jeuxclean.csv", "w", newline="") as csvfile:
            print("Suppresion du pouce bleu en cours, veuillez patienter.")
            writer = csv.writer(csvfile)
            writer.writerows(rows)
            print("Le pouce bleu a été supprimé avec succès!")

    def supprime_poucerouge(self, numligneuser):
        """Enlève un pouce rouge au jeu."""
        with open("ressources/bddutilisateurs.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            rows[numligneuser][7] = list(rows[numligneuser][7])
            rows[numligneuser][7] = convertisseur_liste(
                                    rows[numligneuser][7])
            rows[numligneuser][7] = supcrochetliste(rows[numligneuser][7])
            rows[numligneuser][7] = supguillemetliste(
                                    rows[numligneuser][7])

            if str(self.identifiant) not in rows[numligneuser][7]:
                raise ValueError(
                    "Vous n'avez pas encore donné de"
                    f" pouce rouge à {self.nom}."
                )

            else:
                rows[numligneuser][7].remove(str(self.identifiant))
                with open("ressources/bddutilisateurs.csv", "w",
                          newline="") as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerows(rows)

        self.poucerouge -= 1

        with open("ressources/jeuxclean.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
            for i in range(len(rows)):
                if int(rows[i][0]) == self.identifiant:
                    numligne = i
                    break
        rows[numligne][8] = self.poucerouge

        with open("ressources/jeuxclean.csv", "w", newline="") as csvfile:
            print("Supression du pouce rouge en cours, veuillez patienter.")
            writer = csv.writer(csvfile)
            writer.writerows(rows)
            print("Le pouce rouge a été supprimé avec succès!")

    def recommendation_pouce(self):
        """Affiche le pourcentage de pouces bleus qu'un jeu a reçu."""
        if self.poucebleu + self.poucerouge == 0:
            print(f"{self.nom} a obtenu 0  % de pouces bleu!")
        reco = round((self.poucebleu) / (
            self.poucebleu + self.poucerouge) * 100, 2)
        print(f"{self.nom} a obtenu " + str(reco) + " % de pouces bleu!")

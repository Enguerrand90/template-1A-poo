import csv
from project.invite import Invite


class Utilisateurconnecte(Invite):
    """Modélise un utilisateur connecté.
       connecté

    Envoie un message d'erreur dans le cas où la wishlist n'est pas une
    liste de chaîne de caractères

    Parameters
    ----------
    wishlist : list[str]
        Wishlist d'un utilisateur connecté.
    """

    def __init__(
        self,
        userid,
        pseudo,
        age,
        wishlist,
        listenote,
        listepoucebleu,
        listepoucerouge,
        st=1,
    ):
        if not isinstance(wishlist, list):
            raise TypeError("La wishlist doit être une liste.")
        for i in wishlist:
            if not isinstance(i, str):
                raise TypeError(
                    "Tous les éléments de la wishlist "
                    "doivent être des chaînes de caractères."
                )
        self.age = age
        self.pseudo = pseudo
        self.userid = userid
        self.wishlist = wishlist
        self.listenote = listenote
        self.listepoucebleu = listepoucebleu
        self.listepoucerouge = listepoucerouge
        self.st = st

    def convertisseur_wishlist(self):
        """Convertit une wishlist en une autre wishlist avec le bon format.

        Returns:
        ----------
            list: La wishlist avec le bon format.
        """
        resultat = []
        mot = ""
        for caractere in self.wishlist:
            if caractere == ",":
                resultat.append(mot.strip())
                mot = ""
            else:
                mot += caractere

        resultat.append(mot.strip())
        return resultat

    def supcrochetwishlist(self):
        """Enlève les crochet de la wishlist après la première conversion.
        """
        n = len(self.wishlist)
        if self.wishlist == []:
            return self.wishlist
        elif len(self.wishlist) == 1:
            k = len(self.wishlist[0])
            self.wishlist[0] = self.wishlist[0][0: k - 1]
        k = len(self.wishlist[n - 1])
        self.wishlist[0] = self.wishlist[0][1:]
        self.wishlist[n - 1] = self.wishlist[n - 1][: k - 1]
        return self.wishlist

    def supguillemetwishlist(self):
        """Enlève les guillemets en trop de chaque éléments de la wishlist.
        """
        n = len(self.wishlist)
        for i in range(n):
            k = len(self.wishlist[i])
            self.wishlist[i] = self.wishlist[i][1: k - 1]
        return self.wishlist

    def afficher_wishlist(self):
        """Affiche la wishlist de l'utilisateur."""
        print(f"Voici votre wishlist!\n {self.wishlist}")

    def ajouter_wishlist(self, nom):
        """Ajoute un jeu dans la wishlist de l'utilisateur.

        Lève un message d'erreur dans le cas où le type n'est pas un str,
        ou dans le cas où le jeu est déjà dans la wishlist.

        Parameters
        ----------
        nom : str
            Nom du jeu à ajouter dans la wishlist.

        """
        if not isinstance(nom, str):
            raise TypeError(
                "Le nom du jeu à ajouter doit "
                "être une chaîne de caractères."
            )
        if nom in self.wishlist:
            raise ValueError(
                "Le jeu que vous souhaitez "
                "ajouter est déjà dans votre wishlist."
            )
        with open("ressources/jeuxclean.csv", "r") as file:
            print("Recherche en cours. Veuilez patienter.")
            reader = csv.reader(file)
            reader = list(reader)
            c = 0
            for row in reader:
                if row[1] == nom:
                    self.wishlist.append(nom)
                    if "" in self.wishlist:
                        self.wishlist.remove("")
                    with open(
                        "ressources/bddutilisateurs.csv", "r", newline=""
                    ) as infile:
                        reader = csv.reader(infile)
                        rows = list(reader)
                    rows[self.numligneuser(
                        int(self.userid))][3] = self.wishlist
                    with open(
                        "ressources/bddutilisateurs.csv", "w", newline=""
                    ) as csvfile:
                        writer = csv.writer(csvfile)
                        writer.writerows(rows)
                    print("Le jeu a été ajouté avec succès!")
                    c += 1
                    break
            if c == 0:
                print("Le jeu que vous recherchez n'est pas"
                      " disponible.")

    def supprimer_wishlist(self, nom):
        """Enlève un jeu dans la wishlist de l'utilisateur.

        Lève un message d'erreur dans le cas où le type n'est pas un str,
        ou dans le cas où le jeu n'est pas dans la wishlist.

        Parameters
        ----------
        nom : str
            Nom du jeu à supprimer de la wishlist.

        """
        if not isinstance(nom, str):
            raise TypeError(
                "Le nom du jeu à supprimer doit "
                "être une chaîne de caractères."
            )
        if nom not in self.wishlist:
            raise ValueError(
                "Le jeu que vous souhaitez "
                "supprimer n'est pas dans votre wishlist."
            )
        self.wishlist.remove(nom)
        with open("ressources/bddutilisateurs.csv", "r", newline="") as infile:
            reader = csv.reader(infile)
            rows = list(reader)
        rows[self.numligneuser(int(self.userid))][3] = self.wishlist
        with open("ressources/bddutilisateurs.csv", "w",
                  newline="") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerows(rows)
        print("Le jeu a été supprimé avec succès!")

    def deconnexion(self):
        """Deconnecte l'utilisateur et le fait revenir au menu principal."""
        self.st = 0
        print("Deconnexion réussie :)")

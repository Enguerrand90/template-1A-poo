# Présentation du dépot

Dans le requirements.txt on met généralement les dépendances nécessaire pour faire tourner son projet. Le package `pipreqs` fait ca pour vous. pour l'installer

```
pip install pipreqs dans un terminal
```

puis

```
pipreqs . --force
```
